import sys
import os

n_runs = 10

for i in range(n_runs):
	print 
	print "#################  run " + str(i+1) + " / " + str(n_runs) + "  #################"
	command = ""
	for w in sys.argv[1:-1]:
		command += w + " "
	command += " -J " + sys.argv[-1] + "_run_" + str(i+1) + ".json"
	print command
	os.system(command)
	print

