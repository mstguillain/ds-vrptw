/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/



/*

A DS-VRTPW solver, implemented using the GSA decision rule described in paper:
"A Multistage Stochastic Programming Approach to the Dynamic and Stochastic VRPTW", Saint-Guillain et al, 2015

A greedy decision rule is also implemented.

Supports different types of instance files: Bent (Bent et al, 2004 & 2007), Rizzo (Saint-Guillain et al, 2017)

*/




#include "VRP_instance.h"
#include "VRP_solution.h"
#include "VRP_scenario.h"
#include "NM.h"
#include "LS_Program.h"
#include "tools.h"
#include <cmath>
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */
#include <getopt.h>
#include <chrono>



bool request_lt(const VRP_request& r1, const VRP_request& r2) {
	if (r1 == r2) return false;

	if (r1.revealTime < r2.revealTime)							// reveal time first
		return true;
	if (r1.revealTime == r2.revealTime) {
		if (r1.l < r2.l)										// end of time window second)
			return true;
		if (r1.l == r2.l) {
			ASSERT(r1.getVertex().getRegion() != r2.getVertex().getRegion(), "no tie breaking !");
			return r1.getVertex().getRegion() < r2.getVertex().getRegion();		// region number to break ties
		}
	}

	return false;
}







float offline_computation_time = -1;
float sec_per_time_unit = -1;

int nb_accepts = 0, nb_rejects = 0;

int nb_iter_feasible = 0;
double current_time_unit = 0;
double elapsed_real_time = 0.0, current_expected_realtime = 0.0, elapsed_nb_iter = 0.0;
clock_t t_nb_iter, t, t_init;
bool force = false;
int waiting_multiple = -1; 
int time_scale = 1;
int capacity_scale = 1;
double req_prob_limit = 0.0;
bool custom_wait = false;
bool optimize_online = true;

string str_curr_status = "";
string json_ouput_filename = "";
json j_best_solutions_evo, j_last_best_sol;

const char* current_solution_file = nullptr;
ofstream current_solution_file_stream;
// output current solution
// CallbackReturnType new_best_callback(Solution_DS_VRPTW& bestSolution, double& best_eval) {
CallbackReturnType time_callback(Solution_SS_VRPTW_CR& bestSolution, double &best_eval, Solution_SS_VRPTW_CR& incumbentSolution, double &incumbent_eval, double elapsed_time) {
	UNUSED(elapsed_time); UNUSED(incumbent_eval); UNUSED(incumbentSolution);
	elapsed_nb_iter = (double)(clock() - t_nb_iter) / CLOCKS_PER_SEC; 
	t_nb_iter = clock();
	double iter_per_sec = nb_iter_feasible / elapsed_nb_iter;
	nb_iter_feasible = 0; 

	current_solution_file_stream.open(current_solution_file, ios::out | ios::trunc); 
	current_solution_file_stream << "Pool size: " << "\tN.A. " << "\tRe-sampling rate: " << "\tN.A. ";
	current_solution_file_stream << "     #it/s: " << setw(10) << iter_per_sec << endl;
	current_solution_file_stream << " #accepts:  " << outputMisc::boolColorExpr(true) << setw(5) << nb_accepts << outputMisc::resetColor() << "     \t#rejects: " << outputMisc::boolColorExpr(false) << setw(5) << nb_rejects << outputMisc::resetColor();
	current_solution_file_stream << "     Time unit: " << setw(6) << current_time_unit << "     Elapsed time: " << setw(5) << elapsed_real_time  << endl << endl;
	current_solution_file_stream << "Evaluation: " << best_eval << " \t(cost: " << bestSolution.getCost() << ")";
	int n_viol = bestSolution.getNumberViolations();
	if (n_viol) current_solution_file_stream << "   #violations: " << n_viol;
	current_solution_file_stream << endl << bestSolution.toStringRealVertexNumbers();
	current_solution_file_stream << endl << "E[requests] = " << setw(6) << bestSolution.getExpectedNumberRevealedRequests() << "  \t#Pot.Req. = " << bestSolution.get_requests_ordered().size() << endl;
	current_solution_file_stream << flush;
	current_solution_file_stream.close();

	return NONE;
}




CallbackReturnType newBestSolution(Solution_SS_VRPTW_CR& bestSolution, double& eval) {
	UNUSED(eval);

	// j_last_best_sol = bestSolution.json_desc();
	double elapsed_time = (double)(clock() - t) / CLOCKS_PER_SEC;
	// double elapsed_time_total = (double)(clock() - t_init) / CLOCKS_PER_SEC;

	// SimulationResults r = scenario_pool->computeExperimentalExpectedCost(bestSolution, R_INFTY);
	// cout << "Cost: " << bestSolution.getCost() << " (" << r.avg_rejected << ") " << "\t Expected delay: " << bestSolution.getExpectedDelay() << " (" << r.avg_delay << ")" << "\t Expected #satisfied: " << bestSolution.getExpectedNumberRevealedRequests() - bestSolution.getCost() << " (" << r.avg_serviced << ")" << endl;
	cout << setw(5) << setprecision(3) << elapsed_time << "\t Cost: " << bestSolution.getCost() << "\t Expected delay: " << bestSolution.getExpectedDelay() << endl;


	// if (json_ouput_filename != "") {
	// 	j_last_best_sol["Status"] = str_curr_status;
	// 	j_last_best_sol["Eval"] = eval;
	// 	j_last_best_sol["Cost"] = bestSolution.getCost();
	// 	j_last_best_sol["nRejects"] = nb_rejects;
	// 	j_last_best_sol["nAccepts"] = nb_accepts;
	// 	j_last_best_sol["avgDelay"] = bestSolution.getAVGserviceDelay();
	// 	j_last_best_sol["TimeUnit"] = current_time_unit;
	// 	j_last_best_sol["ElapsedTime_phase"] = elapsed_time;
	// 	j_last_best_sol["ElapsedTime_total"] = elapsed_time_total;
	// 	j_last_best_sol["nIterFeasible"] = nb_iter_feasible;
	// 	j_best_solutions_evo.push_back(j_last_best_sol);
	// }

	return NONE;
}



// in first offline optimization, we stop at soon as we get a feasible solution
CallbackReturnType enditer_callback_offline(Solution_SS_VRPTW_CR& bestSolution, double &best_eval, Solution_SS_VRPTW_CR& incumbentSolution, double &incumbent_eval) {
	UNUSED(best_eval); UNUSED(incumbent_eval); UNUSED(incumbentSolution);
	if (bestSolution.getNumberViolations() == 0) return STOP_LS;
	nb_iter_feasible++;
	return NONE;
}


// in first offline optimization, we stop at soon as we get a feasible solution
CallbackReturnType enditer_callback_online(Solution_SS_VRPTW_CR& bestSolution, double &best_eval, Solution_SS_VRPTW_CR& incumbentSolution, double &incumbent_eval) {
	UNUSED(bestSolution); UNUSED(best_eval); UNUSED(incumbent_eval); UNUSED(incumbentSolution);
	nb_iter_feasible++;
	return NONE;
}



double evaluation_callback(Solution_SS_VRPTW_CR& solution) {

	if (not solution.isFeasible())
		return solution.getCost() + 1000000 * solution.getNumberViolations() + 10000 * solution.getWeightViolations();

	// cout << endl << "-----------------------" << endl << solution.toString() << solution.toString(true) << endl << "cost: " << solution.getCost() << "    exp delay: " << solution.getExpectedDelay() << endl;
	
	return solution.getCost() * 1000 + solution.getExpectedDelay();
}



void help() {
	cout << "Usage: dsvrptw_gsa_json [--help] " << endl;
	cout << endl 
		<< "Options:" << endl 
		<< "\t--help (-h) : print this help" << endl
		<< "\t--verbose(+/++) : turn output verbose" << endl
		<< "\t--number-runs (-n) N : set the number of runs to be performed before printing avg results (default: 1)" << endl
		<< "\t--output-current-sol (-O) FILE: filename to which continuously ouput the current solution; optional" << endl
		<< "\t--json-ouput-filename (-J) FILE: ouput a JSON summary of the optimisation processes; optional" << endl
		<< endl
		<< "\t--offline-computation-time (-o) N: number N of CPU seconds spent during offline optimization phase" << endl
		<< "\t--sec-per-tu (-t) N: number of CPU seconds allocated to each time unit" << endl
		<< endl
		<< "\t--wait-multiple (-W) N: set the waiting time multiple to be used during optimization; default=30" << endl
		<< "\t--time-scale (-s) N: set the computation time scale to be used during optimization (default: 1.0)" << endl
		<< "\t--capa-scale (-q) N: set the computation capacity/demand scale to be used during optimization (default: 1.0)" << endl
		<< "\t--prob-limit (-L) X.X: set probability lower bound (exluded) at which a potential request is considered (default: > 0.0, all probable requests)" << endl
		<< "\t--force-insertion (-F): when trying to insert an online request, perform optimization if no feasible place" << endl
		<< endl
		<< "\t--no-online-opt (-X): do NOT reoptimize during online phase (even after a forced insertion)" << endl

		<< endl
		<< "\t--graph-file (-g) FILE: for both STGUILLAIN and CANDAELE: the file that contains travel times (and other vehicle related stuffs for CANDAELE)" << endl
		<< "\t--carrier-file (-c) FILE: only for type CANDAELE: the carrier instance file" << endl
		<< "\t--customer-file (-C) FILE: only for type CANDAELE: the customer instance file" << endl
		<< "\t--scenario-file (-S) FILE: only for type CANDAELE: the scenario instance file" << endl
		<< "\t--number-vehicles (-V) N: number of vehicles to be used" << endl
		<< "\t--vehicles-capacity (-Q) N: set the capacity of the vehicle(s); optional if stated in the carrier file" << endl
		<< endl;
}

int main(int argc, char **argv) {
	if (argc < 2) { help(); return 1; }

	VRP_request::request_comparison_lt = &request_lt;
	srand(time(NULL));


	int verbose = 0;	
	string carrier_file = "";
	string customers_file = "";
	string graph_file = "";
	string scenario_file = "";
	int number_vehicles = -1;
	int vehicle_capacity = -1;
	int n_runs = 1;

	static struct option long_options[] = {
		{"verbose"		, no_argument		, &verbose,	1},
		{"verbose+"		, no_argument		, &verbose,	2},
		{"verbose++"	, no_argument		, &verbose,	3},
		{"help"			, no_argument		, 0, 'h'},
		{"number-runs"	, required_argument	, 0, 'n'},
		{"output-current-sol"	, required_argument	, 0, 'O'},
		{"json-ouput-filename"	, required_argument	, 0, 'J'},
		
		{"offline-computation-time", required_argument	, 0, 'o'},
		{"sec-per-tu"	, required_argument	, 0, 't'},
		{"time-scale"	, required_argument	, 0, 's'},
		{"capa-scale"	, required_argument	, 0, 'q'},
		{"wait-multiple", required_argument	, 0, 'w'},
		{"prob-limit"	, required_argument	, 0, 'L'},
		{"force"		, no_argument		, 0, 'F'},

		{"no-online-opt", no_argument		, 0, 'F'},

		{"carrier-file"	, required_argument	, 0, 'c'},
		{"customer-file", required_argument	, 0, 'C'},
		{"graph-file"	, required_argument	, 0, 'g'},
		{"scenario-file"	, required_argument	, 0, 'S'},

		{"number-vehicles"	, required_argument	, 0, 'V'},
		{"vehicle-capacity"	, required_argument	, 0, 'Q'},
		{0, 0, 0, 0}
	};
	int c, option_index = 0;
	while ((c = getopt_long (argc, argv, "ho:n:t:s:q:S:w:L:O:J:FXc:C:g:V:Q:", long_options, &option_index)) != -1 ) {
		vector<string> x;
		switch (c) {
			case 'h': help(); return 1;
			case 'n': n_runs = atoi(optarg); break;
			case 'O': current_solution_file = optarg; break;
			case 'J': json_ouput_filename = optarg; break;

			case 'o': offline_computation_time = atof(optarg); break;
			case 't': sec_per_time_unit = atof(optarg); break;
			case 's': time_scale = atof(optarg); break;
			case 'q': capacity_scale = atof(optarg); break;
			case 'w': waiting_multiple = atoi(optarg); break;
			case 'L': req_prob_limit = atof(optarg); break;
			case 'F': force = true; break;

			case 'X': optimize_online = false; break;
			
			case 'c': carrier_file = optarg; break;
			case 'C': customers_file = optarg; break;
			case 'g': graph_file = optarg; break;
			case 'S': scenario_file = optarg; break;
			case 'V': number_vehicles = atoi(optarg); break;
			case 'Q': vehicle_capacity = atoi(optarg); break;

			case '?': abort();
		}
	}
	
	// _ASSERT_(waiting_multiple == -1, "not implemented yet");

	if (offline_computation_time < 0) cout << "ERROR: option --offline-computation-time (-o) must be set" << endl, abort();
	if (sec_per_time_unit < 0 and optimize_online) cout << "ERROR: option --sec-per-tu (-s, #seconds per time unit) must be set" << endl, abort();
	if (sec_per_time_unit < 0.5 and optimize_online) cout << "ERROR: option --sec-per-tu (-s, #seconds per time unit) must be at least 0.5 (given: " << sec_per_time_unit << ")" << endl, abort();
	
	if (req_prob_limit < 0.0 or req_prob_limit >= 1.0) cout << "ERROR: --prob-limit must be 0.0 ≤ x < 1.0 (given: " << req_prob_limit << ")" << endl, abort();

	if (sec_per_time_unit >= 0.5 and not optimize_online) cout << "ERROR: --sec-per-tu  and  --no-online-opt  cannot be set together!" << endl, abort();


	if (carrier_file == "") cout << "ERROR: if instance type is set to CANDAELE, the carrier instance file must be specified with --carrier-file (-c) FILE" << endl, abort();
	if (customers_file == "") cout << "ERROR: if instance type is set to CANDAELE, the customer instance file must be specified with --customer-file (-C) FILE" << endl, abort();
	if (graph_file == "") cout << "ERROR: if instance type is set to CANDAELE, the graph graph file must be specified with --graph-file (-g) FILE" << endl, abort();
	if (scenario_file == "") cout << "ERROR: if instance type is set to CANDAELE, the scenario instance file must be specified with --scenario-file (-S) FILE" << endl, abort();

	custom_wait = waiting_multiple > 0;

	cout << "Instance files: " << endl << "  " << carrier_file << endl << "  " << graph_file << endl << "  " << customers_file << endl << "  " << scenario_file << endl;
	VRP_instance * I;
	


	vector<json> j_best_sols_per_runs;
	for (int i = 0; i < n_runs; i++) {

		if (n_runs > 1)
			cout << "################# run " << i +1 << "/" << n_runs <<  " #################" << endl << endl;
		// I->clearAppearedOnlineRequests();



		// VRP_instance * I = & VRP_instance_file::readInstanceFile_DS_VRPTW__Candaele(carrier_file, graph_file, customers_file, scenario_file, vehicle_capacity);
		I = & VRP_instance_file::readInstanceFile_Hybrid_DS_VRPTW_CR__Candaele(carrier_file, graph_file, customers_file, scenario_file, vehicle_capacity, 1);
		// I = & VRP_instance_file::readInstanceFile_SSVRPTW_CR__Candaele(string(carrier_file), string(graph_file), string(customers_file), vehicle_capacity, 1);
		I->setNoHorizonTW();
		
		
		cout << I->toString() << endl;
		cout << I->toStringVehicleTypes() << endl;
		// cout << I->toStringCoords(true) << endl;
		// cout << I->toString_TravelTimes() << endl;
		// cout << I->toStringProbasTS(true) << endl << endl;
		// cout << I->toStringInstanceMetrics() << endl;
		cout << "Computation scale time = " << time_scale << endl;
		cout << "Computation scale capacity = " << capacity_scale << endl;
		cout << "Pot. req. proba limit: " << req_prob_limit << " (max request proba = " << I->getMaxReqProb() << ")" << endl;

		if (custom_wait)
			cout << "Wait. time multiple:\t" << setw(3) << waiting_multiple << endl;
		else 
			cout << "RELOCATION_ONLY" << endl;

		// cout << I->toStringPotentialOnlineRequests();

		Solution_SS_VRPTW_CR s(*I, R_INFTY, time_scale, capacity_scale, req_prob_limit);
		// Solution_SS_VRPTW_CR s(*I, R_CAPA, time_scale, capacity_scale, req_prob_limit);

		s.dontConsiderServiceTimeAtExpectationCalculus();
		s.setComputeExpectedDelay();
		s.setUpdateAVGdelays();
		if (custom_wait) {
			s.setWaitingStrategy(DRIVE_FIRST_DELAY_LAST);
			s.setWaitingTimeMultiple(waiting_multiple);
			s.setInitialWaitingTime(waiting_multiple);
		}
		else {
			s.setWaitingStrategy(DRIVE_FIRST_DELAY_LAST);
			// s.setWaitingStrategy(RELOCATION_ONLY);
			s.setInitialWaitingTime(0);	
		}


		if (I->getNumberVehicles() < 0 and number_vehicles < 0) cout << "ERROR: Required argument: --number-vehicles (-n) N" << endl, abort();
		if (number_vehicles != -1)
			s.addRoutes(number_vehicles);
		else
			s.addRoutes(I->getNumberVehicles());
			
		
		s.generateInitialSolution(true);		// here all the requests with prebin probability > 0 will be inserted


		// Neighborhood manager & LS Program
		// NeighborhoodManager<Solution_SS_VRPTW_CR> nm_gsa(s, DS_VRPTW_GSA);
		VRP_type vrp_type = DS_VRPTW_GSA_SIMPLE;
		if (custom_wait)
			vrp_type = SS_VRPTW_CR;
		int minIncrement = 0, maxIncrement = 0;
		if (custom_wait)
			minIncrement = waiting_multiple, maxIncrement = 3*waiting_multiple;
		


		NeighborhoodManager<Solution_SS_VRPTW_CR> nm_gsa(s, vrp_type, I->getHorizon(), minIncrement, maxIncrement);
		NeighborhoodManager<Solution_SS_VRPTW_CR> nm_vrptw(s, VRPTW);
		LS_Program_SimulatedAnnealing<Solution_SS_VRPTW_CR,NeighborhoodManager<Solution_SS_VRPTW_CR>> lsprog;


		if (current_solution_file != nullptr) 
			lsprog.set_Time_Callback(time_callback, 0.5);



		nb_accepts = 0; nb_rejects = 0;
		nb_iter_feasible = 0;

		t_init = clock(); 
		t = clock(); 
		t_nb_iter = clock();





		// OFFLINE COMPUTATION ##################################################################################################################
		str_curr_status = "find_feasible";
		if (s.getNumberViolations()) {
			cout << "Offline computation: looking for feasible solution (max " << offline_computation_time << "s) ..." << endl << flush;		

			lsprog.setTimeOut(offline_computation_time);
			lsprog.set_EndOfIteration_Callback(enditer_callback_offline);
			lsprog.run(s, nm_vrptw, numeric_limits<int>::max(), 5.0, 0.995, verbose >= 2);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
			
			if (!s.getNumberViolations())
				cout << "   Found !" << endl;
		}
		if (s.getNumberViolations()) {
			cout << "No initial feasible solution found ! Abording." << endl;
			abort();
		}


		// OFFLINE COMPUTATION : optimize #######################################################################################################
		// if (json_ouput_filename != "") 
			lsprog.set_NewBestSolution_Callback(newBestSolution);

		elapsed_real_time = (double)(clock() - t) / CLOCKS_PER_SEC;
		
		if (verbose >= 2) cout << s.toStringRealVertexNumbers(verbose >= 3);

		// s.setCurrentTime(0);
		lsprog.setTimeOut(offline_computation_time - elapsed_real_time);
		
		str_curr_status = "offline_optimize_GSA";
		cout << "Offline computation: searching for anticipative actions (" << offline_computation_time - elapsed_real_time << "s) ..." << endl << flush;
		
		if (current_solution_file != nullptr) 
			lsprog.set_Time_Callback(time_callback, 0.5);
		
		lsprog.set_Evaluation_Callback(evaluation_callback);
		lsprog.set_EndOfIteration_Callback(enditer_callback_online);
		lsprog.run(s, nm_gsa, numeric_limits<int>::max(), 1.0, 0.95, verbose >= 2);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
		cout << "   Done." << endl;
		if (verbose) cout << "New initial solution:" << endl << s.toStringRealVertexNumbers(verbose >= 3);



		// ONLINE PART ##########################################################################################################################
		// cout << "Real scenario:" << endl << I->toString_AppearingOnlineRequests() << endl;

		// current_time_unit = 0.0;
		current_time_unit = 1.0;
		elapsed_real_time = 0.0; 
		current_expected_realtime = 0.0;
		t = clock();

		Solution_SS_VRPTW_CR s_force = s;
		NeighborhoodManager<Solution_SS_VRPTW_CR> nm_force(s_force, DS_VRPTW_GSA);

		str_curr_status = "online";

		cout << endl << "Starting simulation." << endl;

		if (verbose) cout << fixed << "Time Unit    CPU Time    #rej.        Eval      Event(s)" << endl;
		else cout << "Current time unit: " << flush;

		// s.setCurrentTime(1);
		s.setCurrentTime(0);
		auto it = I->getAppearingOnlineRequests().begin();
		while (current_time_unit <= I->getHorizon()) {
		// while (current_time_unit <= 10) {
			current_expected_realtime = (current_time_unit-1) * sec_per_time_unit;
			elapsed_real_time = (double)(clock() - t) / CLOCKS_PER_SEC;
			if (optimize_online)
				_ASSERT_(elapsed_real_time - current_expected_realtime < sec_per_time_unit * 9/10 , "");				// is the system answering fast enough to potential online requests ? 
			if (verbose) cout << endl << setw(9) << setprecision(1) << current_time_unit << setw(12)  << setprecision(2) << elapsed_real_time << setw(9) << nb_rejects << setw(12) << setprecision(2) << s.getCost() << "      "; 											
			else if (current_time_unit == floor(current_time_unit)) cout << setw(5) << current_time_unit << flush;	
			
			if (it != I->getAppearingOnlineRequests().end()) {				// is there still incoming online requests ?
				
				/********************************************** Online requests management phase ***************************************/
				str_curr_status = "online_requests";

				// Are there some online requests at the current time unit ?
				if (verbose) cout << "Online requests:  ";
				while (it != I->getAppearingOnlineRequests().end() && it->reveal_time == current_time_unit-1) {
					bool forced = false;
					const VRP_request& r = * it->request;
					I->set_OnlineRequest_asAppeared(r, it->reveal_time + 1);		// inform the instance object that the request is from now marked as appeared
					// bool success = false;
					bool success = s.tryInsertOnlineRequest(r);
					if (not success and force) { 
						s_force = s;
						// cout << endl << s_force.toString() << endl;
						s_force.insertOnlineRequest(r);
						elapsed_real_time = (double)(clock() - t) / CLOCKS_PER_SEC;
						double remain_computation_time = (current_expected_realtime + (0.5 * sec_per_time_unit)) - elapsed_real_time;
						if (not optimize_online)
							remain_computation_time = 5;
						double forcing_time = min(remain_computation_time / 2, 5.0);
						if (verbose >=3) cout << " ... forcing for " << forcing_time << "s .." ;
						lsprog.setTimeOut(forcing_time);
						lsprog.set_EndOfIteration_Callback(enditer_callback_offline);
						lsprog.run(s_force, nm_force, numeric_limits<int>::max(), 5.0, 0.995, verbose >= 3);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
						
						if (s_force.isFeasible()) {
							nb_accepts++;
							s = s_force;
							success = true; forced = true;
						}
						else {
							nb_rejects++;
							if (verbose >=3) cout << " failed :(  ";
							// cout << endl << s_force.toStringRealVertexNumbers() << endl << s.toStringRealVertexNumbers() << endl;
						}
					}
					else if (not success and not force) {
						nb_rejects++;
					}
					else nb_accepts++;
					if (verbose) cout << outputMisc::boolColorExpr(success) << outputMisc::blueExpr(forced and success) << r.toStringRealVertexNumbers() << outputMisc::resetColor() << "  ";
					it++;
				}


				/********************************************** Optimization phase *****************************************************/

				str_curr_status = "online_optimize";

				elapsed_real_time = (double)(clock() - t) / CLOCKS_PER_SEC;
				double computation_time = (current_expected_realtime + (0.5 * sec_per_time_unit)) - elapsed_real_time;
				computation_time = max(computation_time - 0.1, 0.01);
				// _ASSERT_(computation_time >= sec_per_time_unit / 10, "");	// is there enough remaining time to perform optimization ? 

				// Fix current solution for 1 time unit ahead
				s.setCurrentTime(current_time_unit + 1);
				if (verbose) cout << endl << setw(21)  << setprecision(2) << elapsed_real_time << setw(9) << nb_rejects << setw(12) << setprecision(2) << s.getCost() << "      Solution fixed until time unit " << current_time_unit + 1 << "; optimize for " << computation_time << "s; "; 											
				
				if (optimize_online) {
					// Optimize during a half time unit minus the time spent for handling online requests
					if (verbose >= 3) cout << endl;
					lsprog.setTimeOut(computation_time);
					lsprog.set_Evaluation_Callback(evaluation_callback);
					lsprog.set_EndOfIteration_Callback(enditer_callback_online);
					lsprog.run(s, nm_gsa, numeric_limits<int>::max(), 1.0, 0.95, verbose >= 3);
					// cout << endl << s.toString() << endl;
				}

			} else {
				cout << endl << "No more online requests to arrive... Terminating." << endl;
				break;
			}

			current_time_unit += 0.5;
		}

		if (verbose) cout << endl << s.toStringRealVertexNumbers(verbose >= 3) << endl << endl;

		cout << endl << "Average service delay: " << s.getAVGserviceDelay() << " (" << s.getNumberServicedRequests() << " serviced)" << endl << endl;
		cout << "Number of rejected requests: \t" << nb_rejects << " / " << I->getAppearingOnlineRequests().size() << endl;
		cout << "Total traveled distances:" << endl;
		for (const VRP_VehicleType* veh : I->getVehicleTypes()) {
			double dist = s.getTraveledDistancePerVehicleType(*veh);
			cout << "    " << veh->toString() << ": \t" << dist << " km \t";
			cout << "( " << s.getNumberNonEmptyRoutes(*veh) << " out of " << s.getNumberRoutes(*veh) << " vehicle used)" << endl;
		}
		cout << endl;
		


		if (json_ouput_filename != "") {
			json j = s.json_desc();
			
			j["_Evolution_Incumbents"] = j_best_solutions_evo;
			j["nAccepts"] = nb_accepts;
			j["nRejects"] = nb_rejects;
			j["avgDelay"] = s.getAVGserviceDelay();

			for (const VRP_VehicleType* veh : I->getVehicleTypes())
				j["TotalTravelDistance" + veh->getName()]= s.getTraveledDistancePerVehicleType(*veh);
			
			j_best_solutions_evo = {};
			j_best_sols_per_runs.push_back(j);
		}



		// delete I;

	}



	/* Writing solution JSON file */
	if (json_ouput_filename != "") {
		cout << endl << "Writting JSON solution file...";
		ofstream json_output_file;
		json j; 
		time_t now = chrono::system_clock::to_time_t(chrono::system_clock::now());
		j["CurrentDateTime"] = ctime(& now);
		j["Algo"] = "2s-GSA";
		j["TimeScale"] = time_scale;
		j["CapacityScale"] = capacity_scale;
		j["WaitingTimeMultiple"] = "RELOCATION_ONLY";
		if (custom_wait)
			j["WaitingTimeMultiple"] = waiting_multiple;
	
		j["ForceInsertion"] = force;

		j["NumberVehicles"] = number_vehicles;
		j["SecdonsPerTU"] = sec_per_time_unit;
		j["NumberOfRuns"] = n_runs;

		j["Instance_Files_Graph"] = graph_file;
		j["Instance_Files_Carrier"] = carrier_file;
		j["Instance_Files_Customers"] = customers_file;
		j["Instance_Files_Scenario"] = scenario_file;

		j["_Best_Solutions_Per_Run"] = j_best_sols_per_runs;

		json_output_file.open(json_ouput_filename, ofstream::out | ios::trunc); 
		json_output_file << j.dump(4);
		json_output_file.close();
		cout << "    Done." << endl;
	}
}



