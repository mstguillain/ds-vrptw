#include "VRP_instance.h"
#include "VRP_solution.h"
#include "VRP_scenario.h"
#include "NM.h"
#include "LS_Program.h"
#include "tools.h"
#include <cmath>
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */
#include <getopt.h>



VRP_ScenarioPool<Scenario_SS_DS_VRPTW_CR, Solution_DS_VRPTW> *scenarioPool;

double evaluation_callback_GSA(Solution_DS_VRPTW& solution) {
	
	if (solution.getNumberViolations())
		return 10000 + solution.getCost() + 10000 * solution.getWeightViolations() + 1000000 * solution.getNumberViolations();

	// SimulationResults r = scenarioPool->evalSolution_GSA(solution);
	SimulationResults r = scenarioPool->computeExperimentalExpectedCost(solution, R_GSA, false, false);

	// cout << "Eval callback called - Eval : " << eval << solution.toString() << endl;
	return r.avg_rejected;
}

int nb_iter = 0;
CallbackReturnType enditer_callback(Solution_DS_VRPTW& bestSolution, double &best_eval, Solution_DS_VRPTW& incumbentSolution, double &incumbent_eval) {
	nb_iter++;
	if (nb_iter == 200) {
		cout << endl << "RESAMPLING POOL" << endl;
		scenarioPool->reSamplePool(0);
		SimulationResults r  = scenarioPool->computeExperimentalExpectedCost(bestSolution, R_GSA, false, false);
		best_eval = r.avg_rejected;
		incumbentSolution = bestSolution;
		incumbent_eval = best_eval;
		nb_iter = 0;
	}
	return NONE;
}

int main(int argc, char **argv) {

	if (argc < 4) {
		cout << "Usage: test carrier_file graph_file customers_file" << endl;
		return 1;
	}
	string carrier_file = argv[1];
	string graph_file = argv[2];
	string customers_file = argv[3];
	// string scenario_file = argv[4];

	srand(time(NULL));

	VRP_instance * I = nullptr;
	I = & VRP_instance_file::readInstanceFile_SSVRPTW_CR__Candaele(carrier_file, graph_file, customers_file);

	
	cout << endl << endl << "Instance files: " << carrier_file << " " << graph_file << " " << customers_file << endl;
	
	cout << I->toString() << endl;
	// // cout << I->toStringCoords(true) << endl;
	cout << I->getVehicleType().toString_TravelTimes() << endl;
	cout << I->getVehicleType().toString_Distances() << endl;
	cout << I->toStringProbasTS() << endl << endl;
	// // cout << I->toStringInstanceMetrics() << endl;

	RecourseStrategy strategy = R_CAPA;
	Solution_SS_VRPTW_CR s(*I, strategy);

	// cout << I->toString_AppearingOnlineRequests() << endl;
	// cout << I->toStringVehicleTypes() << endl;
	// cout << I->toStringPotentialOnlineRequests() << endl;


	// Solution_DS_VRPTW s(*I);
	// const VRP_VehicleType* van;
	// const VRP_VehicleType* bicycle;
	// for (const VRP_VehicleType* veh_type : I->getVehicleTypes()) {
	// 	if (veh_type->getName() == "Van") van = veh_type;
	// 	else if (veh_type->getName() == "Bicycle") bicycle = veh_type;
	// 	else _ASSERT_(false, "");
	// }
	// for (const VRP_vertex* depot : I->getDepotVertices()) {
	// 	if (depot->getIdInstance() == 1) s.addRoute(*depot, *van, 3);
	// 	else if (depot->getIdInstance() == 2) s.addRoute(*depot, *bicycle, 5);
	// 	else _ASSERT_(false, "");
	// }
	// // s.setWaitingStrategy(DRIVE_FIRST_DELAY_LAST);
	// // s.setWaitingStrategy(WAIT_FIRST);
	// s.setWaitingStrategy(RELOCATION_ONLY);
	// cout << "Initial solution:" << endl << s.toString(false);
	// cout << s.toString(true) << endl;
}




