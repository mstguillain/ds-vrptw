/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/



/*

A DS-VRTPW solver, implemented using the GSA decision rule described in paper:
"A Multistage Stochastic Programming Approach to the Dynamic and Stochastic VRPTW", Saint-Guillain et al, 2015

A greedy decision rule is also implemented.

Supports different types of instance files: Bent (Bent et al, 2004 & 2007), Rizzo (Saint-Guillain et al, 2017)

*/




#include "VRP_instance.h"
#include "VRP_solution.h"
#include "VRP_scenario.h"
#include "NM.h"
#include "LS_Program.h"
#include "tools.h"
#include <cmath>
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */
#include <getopt.h>
#include <chrono>



void help() {
	cout << "Usage: dsvrptw_gsa_json [--help] " << endl;
	cout << endl 
		<< "Options:" << endl 
		<< "\t--help (-h) : print this help" << endl
		<< "\t--number (-n) N : number of scenarios (instances) to generate (default: 10)" << endl
		<< "\t--graph-file (-g) FILE: the file that contains travel times (and other vehicle related stuffs) format: CANDAELE" << endl
		<< "\t--carrier-file (-c) FILE: only for type CANDAELE: the carrier instance file" << endl
		<< "\t--customer-file (-C) FILE: only for type CANDAELE: the customer instance file" << endl
		<< "\t--scenario-2017 (-X) FILE: only for type CANDAELE: the scenario instance file" << endl

		<< "\t--scenario-file-prefix (-S) FILE: only for type CANDAELE: the path/prefix of the scenario instance files that will be written" << endl
		<< endl;
}

int main(int argc, char **argv) {

	if (argc < 2) { help(); return 1; }

	string carrier_file = "";
	string customer_file = "";
	string graph_file = "";
	string scenario_file = "";
	string scenarios_2017_filename = "";
	int number = 1;

	static struct option long_options[] = {
		{"help"			, no_argument		, 0, 'h'},
		{"number"		, required_argument	, 0, 'n'},

		{"carrier-file"	, required_argument	, 0, 'c'},
		{"customer-file", required_argument	, 0, 'C'},
		{"graph-file"	, required_argument	, 0, 'g'},
		{"scenario-file-prefix"	, required_argument	, 0, 'S'},

		{"scenario-2017"	, required_argument	, 0, 'X'},

		{0, 0, 0, 0}
	};
	int c, option_index = 0;
	while ((c = getopt_long (argc, argv, "hn:S:c:C:g:X:", long_options, &option_index)) != -1 ) {
		vector<string> x;
		switch (c) {
			case 'h': help(); return 1;
			case 'n': number = atoi(optarg); break;
			
			case 'c': carrier_file = optarg; break;
			case 'C': customer_file = optarg; break;
			case 'g': graph_file = optarg; break;
			case 'X': scenarios_2017_filename = optarg; break;

			case 'S': scenario_file = optarg; break;

			case '?': abort();
		}
	}
	

	if (carrier_file == "") cout << "the carrier instance file must be specified with --carrier-file (-c) FILE" << endl, abort();
	if (customer_file == "") cout << "the customer instance file must be specified with --customer-file (-C) FILE" << endl, abort();
	if (graph_file == "") cout << "the graph graph file must be specified with --graph-file (-g) FILE" << endl, abort();
	if (scenario_file == "") cout << "the scenario instance filename must be specified with --scenario-file (-S) FILE" << endl, abort();
	if (scenarios_2017_filename != "" and number > 1) cout << "error !" << endl, abort();

	cout << "Instance files: " << endl << "  " << carrier_file << endl << "  " << graph_file << endl << "  " << customer_file << endl << "  " << scenario_file << endl;
	
	srand(time(NULL));

	VRP_instance I = VRP_instance_file::readInstanceFile_SSVRPTW_CR__Candaele(carrier_file, graph_file, customer_file, -1, 1, "SS-DS");
	
	cout << I.toString() << endl;
	// cout << I.toStringProbasTS(true) << endl << endl;
	cout << I.toStringPotentialRequests(true);
	// cout << I->toStringPotentialOnlineRequests();


	VRP_ScenarioPool<Scenario_SS_DS_VRPTW_CR, Solution_DS_VRPTW> * scenarioPool;	

	json j_scenarios; 
	if (scenarios_2017_filename != "") {
		cout << "   --> Based on scenarios from " << scenarios_2017_filename << endl;
		std::ifstream jfile_scenarios;
		jfile_scenarios.open(scenarios_2017_filename);
		jfile_scenarios >> j_scenarios; jfile_scenarios.close();
		scenarioPool = new VRP_ScenarioPool<Scenario_SS_DS_VRPTW_CR, Solution_DS_VRPTW>(I, j_scenarios, SS_VRPTW_CR);
	} 
	else 
		scenarioPool = new VRP_ScenarioPool<Scenario_SS_DS_VRPTW_CR, Solution_DS_VRPTW>(I, number, SS_VRPTW_CR);
	// cout << scenarioPool.toString(true) << endl;

	cout << endl << "Writting JSON scenario files..." << endl;
	int i = 1;
	for (const Scenario_SS_DS_VRPTW_CR & scenario : scenarioPool->getScenarios()) {

		stringstream filename_stream;
		filename_stream << scenario_file << "_" << i++ << ".json";
		cout << "\t" << filename_stream.str() << endl;

		json j;
		j["FileType"] = "Scenario";
		vector<json> requests;

		for (const VRP_instance::RequestAttributes & req_attr : scenario.getRequestSequence())  {
			// string vertex_info = req_attr.vertex->toStringRealVertexNumbers(), req_info = req_attr.request->toStringRealVertexNumbers(true);
			// cout << "\t" << "Time " << setw(4) << req_attr.reveal_time << " (time slot " << setw(3) << req_attr.time_slot << ") ~> Request at vertex " << vertex_info << "  req. infos: " << req_info << "  (appeared flag =" << req_attr.request->hasAppeared() << ")" << endl;

			json req;
			req["Node"] = req_attr.vertex->getIdInstance();
			req["Demand"] = (int) req_attr.request->demand;
			req["RequestId"] = req_attr.vertex->getIdInstance() * 100 + req_attr.request->getRevealTS();
			
			req["RevealTime"] = (int) req_attr.reveal_time;
			// _ASSERT_(req_attr.request->e <= req_attr.reveal_time <= req_attr.request->l, "");
			_ASSERT_(req_attr.reveal_time <= req_attr.request->l, req_attr.reveal_time << " > " << req_attr.request->l);
			_ASSERT_(req_attr.request->l <= 2*I.getHorizon(), req_attr.request->l);
			
			req["ServiceDuration"] = (int) req_attr.request->add_service_time;
			_ASSERT_(req_attr.request->getRevealTS() <= I.getNumberTimeSlots(), req_attr.request->getRevealTS() << " > " << I.getNumberTimeSlots());
			
			req["TimeSlot"] = req_attr.request->getRevealTS();
			json tw; 
			tw["start"] = (int) req_attr.request->e; 
			tw["end"] = (int) req_attr.request->l;
			req["TimeWindow"] = tw;

			requests.push_back(req);
		}

		j["Requests"] = requests;

		// cout << j.dump(4);
		ofstream json_output_file;
		json_output_file.open(filename_stream.str(), ofstream::out | ios::trunc); 
		json_output_file << j.dump(4);
		json_output_file.close();
	}



	cout << "Done." << endl;
	
	

}



